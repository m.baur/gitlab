import ListAgents from './list_agents.vue';
import CreateAgent from './create_agent.vue';
import ShowAgent from './show_agent.vue';

export { ListAgents, CreateAgent, ShowAgent };
